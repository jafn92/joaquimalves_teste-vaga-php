@extends('layouts.app')

@section('content')
 <h3>Categorias</h3>
 <div>
  @forelse($categorias as $categoria)
   <div class="card" style="width: 18rem;">
    <img class="card-img-top" src=".../100px180/?text=Image cap" alt="Card image cap"/>
       <div class="card-body">
           <a href="{{ url('/categorias', $categoria->id) }}" class="card-title">{{ $categoria->titulo }}</a>
           <p class="card-text">{{ $categoria->descricao }}</p>
           <a href="{{ route('categorias.edit', $categoria->id) }}" class="btn btn-primary">Editar Categoria</a>
       </div>
   </div>
</div>
<br/>
  @empty
<div>
   <p>There are no categorias to display!</p>
</div>
@endforelse
@stop