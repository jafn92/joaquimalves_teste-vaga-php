@extends('layouts.app')

@section('content')
<div class='col-md-4 col-md-offset-2'>
  <h4>Nova Categoria</h4>
<hr>
  {!! Form::open(['action' => 'CategoriaController@store']) !!}
   @include('categorias.form', ['submitButtonText' => 'Nova Categoria'])
  {!! Form::close() !!}
</div>
@stop