@extends('layouts.app')

@section('content')

<div class='col-md-6 col-md-offset-3'>
  <h1>Editar Post</h1>

<hr>
  
  {!! Form::model($categoria, ['method' => 'PATCH', 'action' => ['CategoriaController@update',$categoria->id]]) !!}
   @include('categorias.form', ['submitButtonText' => 'Editar Categoria'])
  {!! Form::close() !!}
 </div>
@stop