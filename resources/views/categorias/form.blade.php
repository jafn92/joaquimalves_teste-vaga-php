    <div class='form-group'>
        {!! Form::label('titulo', 'Titulo:') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
    </div>

    <div class='form-group'>
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
    </div>

    <div class='form-group'>
    {!! Form::label('status', 'Status:') !!}
    {!! Form::checkbox('status', null, ['class' => 'form-control']) !!}
    </div>

    <div class='form-group'>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-lg btn-success form-control']) !!}
    </div>