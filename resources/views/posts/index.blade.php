@extends('layouts.app')

@section('content')
 <h3>Posts</h3>
 <div>
  @forelse($posts as $post)
   <div class="card" style="width: 18rem;">
    <img class="card-img-top" src=".../100px180/?text=Image cap" alt="Card image cap"/>
       <div class="card-body">
           <a href="{{ url('/posts', $post->id) }}" class="card-title">{{ $post->titulo }}</a>
           <p class="card-text">{{ $post->descricao }}</p>
           <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary">Editar Post</a>
       </div>
   </div>
</div>
<br/>
  @empty
<div>
   <p>There are no posts to display!</p>
</div>
@endforelse
@stop