@extends('layouts.app')

@section('content')
<div class='col-md-4 col-md-offset-2'>
  <h4>Novo Post</h4>
<hr>
  {!! Form::open(['action' => 'PostController@store']) !!}
   @include('posts.form', ['submitButtonText' => 'Novo Post'])
  {!! Form::close() !!}
</div>
@stop