@extends('layouts.app')

@section('content')

<div class='col-md-6 col-md-offset-3'>
  <h1>Editar Post</h1>

<hr>
  
  {!! Form::model($post, ['method' => 'PATCH', 'action' => ['PostController@update',$post->id]]) !!}
   @include('posts.form', ['submitButtonText' => 'Editar Post'])
  {!! Form::close() !!}
 </div>
@stop