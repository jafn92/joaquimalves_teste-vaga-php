<?php

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('categorias')->truncate();
        $this->createCategorias();
    }

    private function createCategorias()
    {
        Categoria::create([
            'titulo' => 'Seed Categoria',
            'status' => 'on',
            'descricao' => 'Insercao de dados por meio de seeds automatizadas...'
        ]);

        $this->command->info('Categoria criada com sucesso');
    }
}
