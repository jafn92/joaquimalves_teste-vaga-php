<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('posts')->truncate();
        $this->command->info('Tabela truncada com sucesso');
        
        $this->createPosts();
    }

    private function createPosts()
    {
        Post::create([
            'titulo' => 'Teste Seed',
            'status' => 'on',
            'descricao' => 'Insercao de dados por meio de seeds automatizadas...',
            'imagem' => 'caminho das imagens'
        ]);

        $this->command->info('Post criado com sucesso');
    }
}
