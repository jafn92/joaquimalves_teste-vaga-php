<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_categoria', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->integer('categoria_id')->unsigned();

            $table->foreign('post_id')
                ->references('id')->on('posts');
                //->onDelete('cascade');
                
            $table->foreign('categoria_id')
                ->references('id')->on('categorias');
                //->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
